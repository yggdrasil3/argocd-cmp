#!/usr/bin/env bash
# This script first looks for all SOPS encrypted files and decrypts them in place.
# Second it generates the Kubernetes manifests depending on the repository type (Helm, kustomize, ...)
#
# Configuration:
#  * DEBUG:      When set to 'true', then the script is executed with 'set -x' and the output is logged to
#                '${DEBUG_PATH}/generate.log'. Is unset by default.
#  * DEBUG_PATH: The path to the log files when 'DEBUG=true' is set. Defaults to "/tmp/argocd_debug_log".

# error/exit on any failure
set -e
ARGOCD_ENV_DEBUG_PATH=${ARGOCD_ENV_DEBUG_PATH:-"/tmp/argocd_debug_log"}

if [ "$ARGOCD_ENV_DEBUG" = true ]; then
    mkdir -p "$ARGOCD_ENV_DEBUG_PATH"
    # tee a script from within the script
    # https://superuser.com/a/1104136
    exec &> >(tee "${ARGOCD_ENV_DEBUG_PATH}/generate.log")
    echo "Working Directory: $(pwd)" >"${ARGOCD_ENV_DEBUG_PATH}/system.log"
    echo "Local Directory listing:" >>"${ARGOCD_ENV_DEBUG_PATH}/system.log"
    ls -l >>"${ARGOCD_ENV_DEBUG_PATH}/system.log"
    echo "Environment variables:" >>"${ARGOCD_ENV_DEBUG_PATH}/system.log"
    printenv >>"${ARGOCD_ENV_DEBUG_PATH}/system.log"
    set -x
fi
# SOPS encrypted files always contains a line starting with 'mac: ENC['
# or mac=ENC.
# With grep we are searching these files recursively and forward only
# the file names to xargs.
# With xargs we instruct SOPS to decrypt the file in place.
grep -ERl 'mac: ENC\[|mac=ENC' . | xargs -I '[]' sops -d exec-file '[]' 'cat {} | sed "s/_encrypted:/:/g" > []'

if test -f "Chart.yaml"; then
    HELM=$(which helm)

    # Collect Helm options from
    # `helm template --help`

    HELM_FLAGS="--validate --include-crds"

    if [[ "$ARGOCD_APP_NAMESPACE" ]]; then
        HELM_FLAGS="${HELM_FLAGS} --namespace=${ARGOCD_APP_NAMESPACE}"
    fi

    if [[ "$KUBE_VERSION" ]]; then
        HELM_FLAGS="$HELM_FLAGS --kube-version=${KUBE_VERSION}"
    fi

    if [[ "${KUBE_API_VERSIONS}" ]]; then
        HELM_API_VERSIONS=""
        for v in ${KUBE_API_VERSIONS//,/ }; do
            HELM_API_VERSIONS="${HELM_API_VERSIONS} --api-versions=$v"
        done
        HELM_FLAGS="${HELM_FLAGS} ${HELM_API_VERSIONS}"
    fi

    if [[ "${ARGOCD_ENV_HELM_VALUES_FILES}" ]]; then
        INTERNAL_HELM_VALUES_FILES=""
        for v in ${ARGOCD_ENV_HELM_VALUES_FILES//,/ }; do
            INTERNAL_HELM_VALUES_FILES="${INTERNAL_HELM_VALUES_FILES} --values=$v"
        done
        HELM_FLAGS="${HELM_FLAGS} ${INTERNAL_HELM_VALUES_FILES}"
    fi

    # Execute Helm
    ${HELM} template ${ARGOCD_APP_NAME} . ${HELM_FLAGS}
elif test -f "kustomization.yaml"; then
    kustomize build --enable-helm .
else
    find . -type f \( -name "*.yml" -o -name "*.yaml" \) -exec sh -c 'echo "---"; cat {}; echo' \;
fi
