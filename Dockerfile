FROM alpine:3@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48 as dependencies

ENV BINARY_INSTALL_DIR=/usr/local/sbin

# hadolint ignore=DL3018
RUN apk --no-cache update && \
    apk --no-cache add curl jq tar && \
    mkdir -p ${BINARY_INSTALL_DIR}

# Get Mozilla/sops binary
RUN curl -L --silent --output ${BINARY_INSTALL_DIR}/sops \
    "$(curl https://api.github.com/repos/getsops/sops/releases/latest | \
    jq -r '.assets[] | select(.name | contains("linux.amd64")) | .browser_download_url' | grep -e 'amd64$')" && \
    chmod +x ${BINARY_INSTALL_DIR}/sops

# Get Kustomize binary
RUN curl -L --silent $(curl https://api.github.com/repos/kubernetes-sigs/kustomize/releases/latest | \
    jq -r '.assets[] | select(.name | contains("linux_amd64")) | .browser_download_url') | \
    tar -xz -C ${BINARY_INSTALL_DIR}/ && \
    chmod +x ${BINARY_INSTALL_DIR}/kustomize

# Get Helm binary
RUN curl -L --silent \
    https://get.helm.sh/helm-$(curl https://api.github.com/repos/helm/helm/releases/latest | jq -r '.tag_name')-linux-amd64.tar.gz | \
    tar -xz -C /tmp/ && \
    cp /tmp/linux-amd64/helm ${BINARY_INSTALL_DIR}/helm && \
    chmod +x ${BINARY_INSTALL_DIR}/helm

# Copy sops ConfigManagementPlugin
COPY sops-cmp.sh ${BINARY_INSTALL_DIR}/sops-cmp.sh
RUN chmod +x ${BINARY_INSTALL_DIR}/sops-cmp.sh

FROM alpine:3@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48 as cmp

ENV BINARY_INSTALL_DIR=/usr/local/sbin

# hadolint ignore=DL3018
RUN apk --no-cache update && \
    apk --no-cache add bash && \
    mkdir -p ${BINARY_INSTALL_DIR}

COPY --from=dependencies ${BINARY_INSTALL_DIR}/sops ${BINARY_INSTALL_DIR}/sops
COPY --from=dependencies ${BINARY_INSTALL_DIR}/kustomize ${BINARY_INSTALL_DIR}/kustomize
COPY --from=dependencies ${BINARY_INSTALL_DIR}/helm ${BINARY_INSTALL_DIR}/helm
COPY --from=dependencies ${BINARY_INSTALL_DIR}/sops-cmp.sh ${BINARY_INSTALL_DIR}/sops-cmp.sh
COPY --chown=999:999 plugin.yaml /home/argocd/cmp-server/config/plugin.yaml

USER 999
