import { Pipeline } from "@gcix/gcix"
import { BuildGitlabContainerCollection } from "@gcix/gcix/lib/container"

const pipeline = new Pipeline()
pipeline.addChildren({
    jobsOrJobCollections: [
        new BuildGitlabContainerCollection({})
    ]
})
pipeline.writeYaml()
